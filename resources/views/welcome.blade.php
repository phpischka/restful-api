<template>
    <div class="container">
        <h1>{{name}}</h1>
        <div class="form-group">
            <input type="text" class="form-control" :class="{ 'is-invalid': $v.name.$error }" @blur="saveName" v-model="name">
            <div class="invalid-feedback" v-if="!$v.name.required">
                Field is required.
            </div>
            <div class="invalid-feedback" v-if="!$v.name.maxLength">
                The name must contain at most {{$v.name. $params.maxLength.max}} letters.
            </div>
        </div>
        <div class="alert alert-danger mt-3" role="alert" v-if="errored">
            Ошибка загрузки данных!<br>
            <ul>
                <li v-for="error in errors">{{error}}</li>
            </ul>
        </div>
        <div class="text-center m-5">
            <div class="spinner-border text-primary" role="status" v-if="loading">
                <span class="visually-hidden">Loading...</span>
            </div>
        </div>
        <div class="row mt-3 mb-3">
            <div class="col-lg-4 mt-3" v-for="deskList in deskLists">
                <div class="card"> 
                    <a href="#" >
                        <h5 class="card-title">{{deskList.name}}</h5>
                    </a>
                </div>
            </div>
        </div>
    </div>
</template>
<script>
    import { required, maxLength } from 'vuelidate/lib/validators';
    
    export default{
        props: [
            'deskId'
        ],
        data() {
            return {
                name: null,
                errored: false,
                errors: [],
                loading: true,
                deskLists: []
            }
        },
        methods:{
            getDesk() {
                axios.get('/api/v1/desks/' + this.deskId)
                .then((response => {
                    this.name = response.data.data.name;
                }))
                .catch(error => {
                    console.log(error);
                    this.errored = true;
                })
                .finally(() => {
                    this.getDeskLists();
                    this.loading = false;
                });
            },
            getDeskLists() {
                axios.get('/api/v1/desk-lists', {
                    params: {
                        desk_id: this.deskId
                    }
                })
                .then((response => {
                        this.deskLists = response.data.data;
                }))
                .catch(error => {
                    console.log(error.response);
                    this.errored = true;
                })
                .finally(() => {
                       this.loading = false;
                });
            },
            saveName(){
                this.$v.$touch();
                if (this.$v.$anyError) {
                    return;
                }
                axios.post('/api/v1/desks/' + this.deskId, {
                    _method: 'PUT',
                    name: this.name
                })
                .then((response => {
                    //this.desk = response.data.data;
                }))
                .catch(error => {
                    console.log(error.response);
                    if (error.response.data.errors.name) {
                        this.errors = [];
                        this.errors.push(error.response.data.errors.name);
                    }
                    this.errored = true;
                })
                .finally(() => {
                    this.loading = false;
                })
                
            }
        },
        mounted() {
            this.getDesk();
        },
        validations: {
            name: {
                required, 
                maxLength: maxLength(255)
            }
        }
    }
</script>
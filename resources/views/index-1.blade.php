<!DOCTYPE html>
<html>
<head>
  <title>My first Vue app</title>
  <script src="https://unpkg.com/vue"></script>
</head>
<body>
  <div id="app">
    <h1 :title="title">My first Vue app</h1>
      
    <template v-if="status == 1">
        <h1>Заголовок 1</h1>
        <p>Параграф к нему</p>
    </template>
    
    <template v-else-if="status == 2">
        <h1>Заголовок 2</h1>
        <p>Параграф к нему</p>
    </template>
    
    <template v-else>
        <h1>Заголовок else</h1>
        <p>Параграф к нему</p>
    </template>
    <h3 v-show="status == 2">Заголовок Show</h3>
    <div class="default" :class="{active:isActive, 'btn-develop':isBtn}">
        <h3>default class</h3>
    </div>
    <div class="default" :class="[isActive ? 'active' : '', 'default']">
        <h3>default class c тернарным опереатором</h3>
    </div>
    <div class="default">
        <h3 :style="{color:color, width:'200px'}">Замена css</h3>
    </div>
    <div class="default">
        <h3>массивы:</h3>
        
        <ul>
            <li v-for="animal in animals">@{{animal}}</li>
        </ul>
        
        <ul>
            <li v-for="user in users">
                <a href="{{user.id}}">@{{user.name}}</a>
            </li>
        </ul>

    </div>
  </div>

  <script>
    var app = new Vue({
      el: '#app',
      data: {
        message: 'Hello Vue22!',
        title: 'This title333',
        status: 2,
        isActive: false,
        isBtn: false,
        color: 'red',
        animals: ['dog', 'monkey', 'cat'],
        users: [
            {id: 1, name: 'Jeyson'},
            {id: 2, name: 'Tom'},
            {id: 3, name: 'Alex'},
        ],
        
      }
    })
  </script>
</body>
</html>
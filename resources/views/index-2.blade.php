<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Trello Analog</title>
        <link rel="stylesheet" href="{{mix('/css/app.css')}}">
    </head>
    <body>
        <div id="app">
            <h3>Form</h3>
            <p v->Длина введенного текста @{{textSearch.length}} символов</p>
            <input type="text" size="100" v-model="textSearch">
            
            <h3>Hashtags</h3>
            <ul>
                <li v-for="hashtag in hashtags">@{{hashtag}}</li>
            </ul>

            <button v-on:click="gethashtags">get hashtags</button>
        </div>
        <script src="{{mix('/js/app.js')}}"></script>
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    </body>
</html>

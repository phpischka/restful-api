<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Trello Analog</title>
        <link rel="stylesheet" href="{{asset('/css/all.min.css')}}">
        <link rel="stylesheet" href="{{mix('/css/app.css')}}">
    </head>
    <body>
        <div id="app">
            <app></app>
        </div>
        <script src="{{mix('/js/app.js')}}"></script>
    </body>
</html>

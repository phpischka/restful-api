require('./bootstrap');

import Vue from 'vue';

Vue.component('app', require('./components/App.vue').default);

const app = new Vue({
    el: '#app',
    data: {
        show: 1,
        message: '100x',
        phones: ['iPhone 7', 'Galaxy S8', 'Nokia N8', 'Xiaomi Mi6'],
        hashtags: [],
        textSearch: '',
        url: {
            hashtags: 'https://dka-develop.ru/api?type=hashtag',
            cities: 'https://dka-develop.ru/api?type=city'
        }
    },
    watch: {
        textSearch: function(){
            if (this.textSearch.length > 10) {
                this.textSearch = 'меняем содержимое поля из кода';
            }
        }
    },
    created: function(){
        console.log(this.url.cities);
    },
    methods: {
        gethashtags: function(){
            axios.get(this.url.cities).then((response) => {
                this.hashtags = response.data;
            });
        },
        alert2(){
             alert(2);
        },
        updateList: function(){
                this.phones.pop();
        }
    }
});